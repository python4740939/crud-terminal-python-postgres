-- Crear una base de datos
CREATE DATABASE crud_python;

-- Conectarse a la base de datos recién creada
\c crud_python;

-- Crear una tabla de usuario
CREATE TABLE usuarios (
    id serial PRIMARY KEY,
    nombre VARCHAR (50),
    apellido VARCHAR (50),
    email VARCHAR (100) UNIQUE
);
