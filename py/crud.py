import psycopg2
import time

def conectar_db():
    connection = None
    intentos = 0
    while intentos < 5:
        try:
            connection = psycopg2.connect(
                host="postgres",
                database="crud_python",
                user="postgres",
                password="12345"
            )
            print("Conexión a la base de datos exitosa")
            break  # Sal del bucle si la conexión es exitosa
        except (Exception, psycopg2.Error) as error:
            print(f"Error al conectar a la base de datos ({intentos + 1}): {error}")
            intentos += 1
            time.sleep(2)  # Espera 2 segundos antes de volver a intentar

    return connection

# Intenta conectar a la base de datos
connection = conectar_db()

# Si la conexión es exitosa, puedes continuar con tu lógica de la aplicación
if connection:
    # Realiza las operaciones CRUD o cualquier otra lógica aquí
    connection.close()

# Función para crear un registro en la tabla
def crear_registro(connection, nombre, apellido, email):
    try:
        cursor = connection.cursor()
        insert_query = "INSERT INTO usuarios (nombre, apellido, email) VALUES (%s, %s, %s)"
        data_to_insert = (nombre, apellido, email)
        cursor.execute(insert_query, data_to_insert)
        connection.commit()
        print("Registro creado con éxito")
    except (Exception, psycopg2.Error) as error:
        print("Error al crear el registro:", error)

# Función para leer todos los registros de la tabla
def leer_registros(connection):
    try:
        cursor = connection.cursor()
        select_query = "SELECT * FROM usuarios"
        cursor.execute(select_query)
        records = cursor.fetchall()
        for row in records:
            print("ID:", row[0])
            print("Nombre:", row[1])
            print("Apellido:", row[2])
            print("Email:", row[3])
            print()
    except (Exception, psycopg2.Error) as error:
        print("Error al leer los registros:", error)

# Función para actualizar un registro en la tabla
def actualizar_registro(connection, id, nuevo_nombre, nuevo_apellido, nuevo_email):
    try:
        cursor = connection.cursor()
        update_query = "UPDATE usuarios SET nombre = %s, apellido = %s, email = %s WHERE id = %s"
        data_to_update = (nuevo_nombre, nuevo_apellido, nuevo_email, id)
        cursor.execute(update_query, data_to_update)
        connection.commit()
        print("Registro actualizado con éxito")
    except (Exception, psycopg2.Error) as error:
        print("Error al actualizar el registro:", error)

# Función para eliminar un registro en la tabla
def eliminar_registro(connection, id):
    try:
        cursor = connection.cursor()
        delete_query = "DELETE FROM usuarios WHERE id = %s"
        cursor.execute(delete_query, (id,))
        connection.commit()
        print("Registro eliminado con éxito")
    except (Exception, psycopg2.Error) as error:
        print("Error al eliminar el registro:", error)

# Función principal
def main():
    connection = conectar_db()
    if connection:
        while True:
            print("Selecciona una opción:")
            print("1. Crear registro")
            print("2. Leer registros")
            print("3. Actualizar registro")
            print("4. Eliminar registro")
            print("5. Salir")
            opcion = input("Opción: ")
            if opcion == '1':
                nombre = input("Nombre: ")
                apellido = input("Apellido: ")
                email = input("Email: ")
                crear_registro(connection, nombre, apellido, email)
            elif opcion == '2':
                leer_registros(connection)
            elif opcion == '3':
                id = input("ID del registro a actualizar: ")
                nuevo_nombre = input("Nuevo nombre: ")
                nuevo_apellido = input("Nuevo apellido: ")
                nuevo_email = input("Nuevo email: ")
                actualizar_registro(connection, id, nuevo_nombre, nuevo_apellido, nuevo_email)
            elif opcion == '4':
                id = input("ID del registro a eliminar: ")
                eliminar_registro(connection, id)
            elif opcion == '5':
                break
            else:
                print("Opción no válida. Intenta de nuevo.")

        connection.close()

if __name__ == "__main__":
    main()
